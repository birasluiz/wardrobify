# Wardrobify

Team:

* Person 1 - Which microservice?
* Abdoul Ba - Hats
* Bira Luiz - Shoes microservice
* Person 2 - Which microservice?

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

I created a hats model and api views for that model. I connected it to the location model in the wardrobe. Then I created react components to match all of it.
