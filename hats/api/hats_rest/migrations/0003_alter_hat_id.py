# Generated by Django 4.0.3 on 2023-08-30 21:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_alter_hat_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hat',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
