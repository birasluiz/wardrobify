import json
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name", "import_href", "bin_number", "closet_name", "bin_size", "id"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["model_name", "bin_number", "id", "manufacturer", "color"]

    def get_extra_data(self, o):
        return {"bin_number": o.bin_number.id}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin_number",
    ]
    encoders = {
        "bin_number": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def shoes_list_view(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)
        try:
            bin_id = content["bin_number"]

            bin = BinVO.objects.get(import_href=f'/api/bins/{bin_id}/')
            content["bin_number"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"error": "Bin number does not exist"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)

        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def shoe_detail_view(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0})

    else:
        content = json.loads(request.body)
        try:
            if "bin_number" in content:
                bin = BinVO.objects.get(id=content["bin_number"])
                content["bin_number"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"error": "Bin number does not exist"},
                status=400,
            )

        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
