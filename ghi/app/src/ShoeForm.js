import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [modelName, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [binNumber, setBinNumber] = useState('');

    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleChangeModelName = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleChangeColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleChangeBinNumber = (event) => {
        const value = event.target.value;
        setBinNumber(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
            data.manufacturer = manufacturer;
            data.model_name = modelName;
            data.color = color;
            data.bin_number = binNumber;
        ;

        const shoesURL = 'http://localhost:8080/api/shoes/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const shoeResponse = await fetch(shoesURL, fetchOptions);
        if (shoeResponse.ok) {
            const newShoe = await shoeResponse.json();
            setManufacturer('');
            setModelName('');
            setColor('');
            setBinNumber('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className='shadow p-4 mt-4'>
                    <h1>Add a Shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeManufacturer} value={manufacturer} placeholder="Manufacturer" required name="manufacturer" type="text" id="manufacturer" className='form-control' />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeModelName} value={modelName} placeholder="Model Name" required name="model_name" type="text" id="model_name" className='form-control' />
                            <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeColor} value={color} placeholder="Color" required name="color" type="text" id="color" className='form-control' />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleChangeBinNumber} value={binNumber} placeholder="Bin Number" required name="bin_number" type="text" id="bin_number" className='form-control'>
                                <option value="">Select a bin</option>
                                {bins.map((bin) => {
                                    return (
                                        <option key={bin.href} value={bin.id}>
                                            {bin.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                            </div>
                            <button className="btn btn-primary">Create Shoe</button>
                        </form>
                    </div>
                </div>
            </div>
    );
}

export default ShoeForm;
