import React, { useEffect } from 'react';

function ShoeList(props) {
    if (!props.shoes || props.shoes.length === 0) {
        return <p>No shoes available.</p>;
    }

    const funRemove=(id)=> {
            fetch('http://localhost:8080/api/shoes/'+id,
            {method:"DELETE"}).then(()=>{

                window.location.reload()


            }).catch((err)=> {
                console.log(err.message)
            })
    }
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map((shoe) => (
                    <tr key={shoe.id}>
                        <td>{shoe.manufacturer}</td>
                        <td>{shoe.model_name}</td>
                        <td>{shoe.color}</td>
                        <td><button className='btn btn-danger' onClick={()=>{funRemove(shoe.id)}}>Delete</button></td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
    }


export default ShoeList;
